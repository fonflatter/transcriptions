const express = require('express')
const fs = require('fs')

const app = express()

const {
  PORT :port = 3000,
  HOST : host = 'localhost',
  TRANSCRIPTION_DIR : transcriptionBaseDir
} = process.env

app.use(express.urlencoded({ extended: false }))
    
const route = /(\d{4})\/(\d{2})\/(\d{2})/

app.all(route, (req, res, next) => {
    const [year,month,day] = Object.values(req.params)
    if (parseInt(year, 10) < 2005 || parseInt(year, 10) > new Date().getFullYear() + 1) {
      return res.status(404).send('Ungültiges Jahr!')
    }

    if (new Date(`${year}-${month}-${day}`).toString() === 'Invalid Date') {
      return res.status(404).send('Ungültiges Datum!')
    }

    const transcriptionDir = `${transcriptionBaseDir}/${year}/${month}/${day}/`
    const transcriptionFiles = fs.existsSync(transcriptionDir) ? fs.readdirSync(transcriptionDir) : []
    transcriptionFiles.sort()

    if (transcriptionFiles.length > 10) {
	return res.status(507).send('Es gibt schon zu viele Transkriptionen!')
    }

    Object.assign(res.locals, {
	year, month, day,
	transcriptionDir,
	transcriptionFiles
    })
    next()
})

app.get(route, (req, res) => {
    const {
	year, month, day,
	transcriptionDir,
	transcriptionFiles
    } = res.locals
    const lastFile = transcriptionFiles.length > 0 ? JSON.parse(fs.readFileSync(transcriptionDir + transcriptionFiles.pop())) : {}
    const { user = '', text = '' } = lastFile

    const firstNumber = Math.ceil(Math.random() * 9)
    const secondNumber = Math.ceil(Math.random() * 9)

    res.send(`
          <form method="POST" style="font-family: sans-serif; max-width: 800px; margin: auto;">
            <p>
                Einen Comic zu transkribieren, ist einfach. Kopiere einfach Dialoge und – falls vorhanden – Geräusche
                und
                Erzählstimme hinter den Namen der lautgebenden Personen. Jede Person kommt in eine eigene Zeile und nach
                jedem Panel
                fügst du eine Leerzeile ein. Wenn du willst, kannst du zusätzlich noch für jedes Panel eine
                Bildbeschreibung in
                zweifachen eckigen Klammer und den Mouseovertext in zweifachen geschweiften Klammern anfügen.
            </p>

        <p>
            Falls Fragen bestehen, schau einfach ins <a href="//fonflatter.de/archiv/">Archiv</a> oder schreib mir eine
            <a href="//fonflatter.de/kontakt/">Mail</a>. Und so.
        </p>
        <hr>
        <p><img src="//fonflatter.de/${year}/fred_${year}-${month}-${day}.png" /></p>
        <p>
        <input type="hidden" name="firstNumber" value="${firstNumber}" />
        <input type="hidden" name="secondNumber" value="${secondNumber}" />
        <label>
          Bitte addiere ${firstNumber} und ${secondNumber}:<br>
          <small>(um Spam zu vermeiden)</small><br>
          <input name="solution" />
        </label>
        </p>
        <p>
        <label>
          Name:<br>
          <small>(optional)</small><br>
          <input name="user" value="${user.replace(/"/g, '&quot;')}" />
        </label>
        </p>
        <p>
        <label>
          Transkription:<br>
          <textarea name="text" cols="50" rows="10">${text.replace(/</g, '&lt;')}</textarea>
        </label>
        </p>
        <button type="submit">speichern</button>
      </form>
`)	
})

app.post(route, (req, res) => {
    const { transcriptionDir, transcriptionFiles } = res.locals
    const fileName = new Date().toISOString() + '.json'
    if (transcriptionFiles.includes(fileName)) {
      return res.status(409).send('Transkription existiert bereits!')
    }

    const {
	firstNumber,
	secondNumber,
	solution,
	user,
	text
    } = req.body

    if (!text || text.trim() === '') {
      return res.status(400).send('Es fehlt der Text!')
    }

    if (!solution || solution.trim() !== 'doof' && parseInt(solution, 10) !== parseInt(firstNumber, 10) + parseInt(secondNumber, 10)) {
      return res.status(400).send('Es fehlt die Lösung!')
    }

    if (!fs.existsSync(transcriptionDir)) {
      fs.mkdirSync(transcriptionDir, { recursive: true })
    }
    
    fs.writeFileSync(transcriptionDir + fileName, JSON.stringify({	user, text }, null, 2))
    res.send('Danke für deine Unterstützung!')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
